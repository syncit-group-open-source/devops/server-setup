bashrc SETUP

```shell
cd
mkdir -pv .config/{bash,vim}
yes | mv {,bk}.bashrc && curl -LJO  https://gitlab.com/syncit-group-open-source/devops/server-setup/-/raw/master/dotfiles/.bashrc
curl -s  https://gitlab.com/syncit-group-open-source/devops/server-setup/-/raw/master/dotfiles/.vimrc > .config/vim/vimrc
```
