#!/bin/bash
[[ $- != *i* ]] && return

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

function parse_git_branch {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="┌─[\[\e[35m\]\u\[\e[m\]@\[\e[32m\]\$(hostname -f)\[\e[m\]]-[\A]-[\[\e[35m\]\w\[\e[m\]] $(parse_git_branch) \n└─[> "


case $TERM in
    xterm*)
            if [ -e /etc/sysconfig/bash-prompt-xterm ]; then
                    PROMPT_COMMAND=/etc/sysconfig/bash-prompt-xterm
            else
        PROMPT_COMMAND='printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/~}"'
            fi
            ;;
    screen)
            if [ -e /etc/sysconfig/bash-prompt-screen ]; then
                    PROMPT_COMMAND=/etc/sysconfig/bash-prompt-screen
            else
        PROMPT_COMMAND='printf "\033]0;%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/~}"'
            fi
            ;;
    *)
            [ -e /etc/sysconfig/bash-prompt-default ] && PROMPT_COMMAND=/etc/sysconfig/bash-prompt-default
        ;;
esac


export EDITOR=vim
export VISUAL=vim
export VIMINIT="source /home/$USER/.config/vim/vimrc"

export PATH=$PATH:/home/$USER/.local/bin

HISTSIZE= HISTFILESIZE=
HISTTIMEFORMAT="%F %T "

## bc ##
alias bc='bc -l'

## ports ##
alias portsn='netstat -tulanp'
alias portss='ss -tulanp'

## colorize commands output for ease of use ##
alias diff='colordiff'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias sgrep='grep --color=auto -rni'
alias ls='ls -F --color=auto -a'
alias lsd='ls --color=auto --group-directories-first -hN'
alias ll='ls -FGlAhp'

# do not delete / or prompt if deleting more than 3 files at a time #
alias rm='rm -I --preserve-root'

# confirmation #
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'

# Parenting changing perms on / #
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# become root #
alias root='sudo -i'
alias su='sudo -i'

## silver searcher ##
alias agsuper='bash -xc '\''ag --color -g $0 --hidden'\''  2>/dev/null'
alias agsuperstr='ag $1 --hidden 2>/dev/null'
alias agsphp='ag $1 --php --hidden 2>/dev/null'
alias agslog='ag $1 --log --hidden 2>/dev/null'

## create parent directories on demand ##
alias mount='mount |column -t'

## ping variations ##
alias fastping='ping -c 100 -s 2'

## resume wget by default##
alias wget='wget -c'

## pass options to free ##
alias meminfo='free -m -l -t'

## ps top ##
alias pscpu='ps -eo pid,ppid,%mem,%cpu,cmd --sort=-%cpu | head'
alias pscpus='ps -eo pid,ppid,%mem,%cpu,comm --sort=-%cpu | head'

## handy short cuts ##
alias h='history'
alias j='jobs -l'
alias c='clear'
alias e='exit'
alias v='vim'

## create parent directories on demand ##
alias mkdir='mkdir -pv'

## octal perms ##
alias ocperms='stat -c "%A %a %n" $1'

## network ##
alias lip='hostname -i'
alias xip='curl -s ifconfig.me -w "\n"'

## termbin paste ##
alias tb='nc termbin.com 9999'

# misc ##
alias tree="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
alias dusage="du -h --max-depth=1 | sort -rh"
alias nocomment='grep -Ev '"^(#|$)"''
alias hg='history|grep '
alias conns='sudo lsof -n -P -i +c 15'


## func ##

if [[ $- == *i* ]]
then
  bind '"\e[A": history-search-backward'
  bind '"\e[B": history-search-forward'
  bind '"\e[C": forward-char'
  bind '"\e[D": backward-char'
fi

## extraction ##
function extract()
{
    if [ -f "$1" ]
        then
        case $1 in
            *.tar.bz2)  tar xvjf "$1"     ;;
            *.tar.gz)   tar xvzf "$1"     ;;
            *.bz2)      bunzip2 "$1"      ;;
            *.rar)      unrar x "$1"      ;;
            *.gz)       gunzip "$1"       ;;
            *.tar)      tar xvf "$1"      ;;
            *.tbz2)     tar xvjf "$1"     ;;
            *.tgz)      tar xvzf "$1"     ;;
            *.zip)      unzip "$1"        ;;
            *.Z)        uncompress "$1"   ;;
            *.7z)       7z x "$1"         ;;
            *)          echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

## compression ##
function targ() {
    tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"
}

function tarb() {
    tar cvjSf "${1%%/}.tar.bz2" "${1%%/}/"
}

function mkzip() {
    zip -r "${1%%/}.zip" "$1"
}


## dirs manipulation ##
function cdtmp()
{
    temp_dir="$(mktemp -d)"
    [[ -d "$temp_dir" ]] || return 1
    (trap "rm -rf \"$temp_dir\"" EXIT; cd "$temp_dir"; bash -i)
}

function mkcd()
{
    mkdir -p "$@"
    cd "$1"
}

up()
{
    dir=""
    if [[ $1 =~ ^[0-9]+$ ]]; then
        x=0
        while [ $x -lt ${1:-1} ]; do
            dir=${dir}../
            x=$(($x+1))
        done
    else
         dir=..
    fi
    cd "$dir";
}
